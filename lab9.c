#include <stdio.h>
void swap(int *a,int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}
int main()
{
    int x,y;
    printf("Enter the two integers:");
    scanf("%d%d",&x,&y);
    printf("Before swapping a=%d b=%d \n",x,y);
    swap(&x,&y);
    printf("After swapping a=%d b=%d \n",x,y);
    return 0;
}