#include <stdio.h>
int main()
{
    FILE *f;
    char ch;
    f=fopen("INPUT.txt","w");
    printf("Enter the data:\n");
    while((ch=getchar())!=EOF)
    {
        putc(ch,f);
    }
    fclose(f);
    printf("\n");
    f=fopen("INPUT.txt","r");
    printf("Contents of the file:\n");
    while((ch=getc(f))!=EOF)
    {
        printf("%c",ch);
    }
    fclose(f);
    return 0;
}
