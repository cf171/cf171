#include<stdio.h>
#include<math.h>
int dis(int,int,int,int);
int main()
{
   int x1,x2,y1,y2,d;
  printf("Enter the x coordinates:");
  scanf("%d %d",&x1,&x2);
  printf("Enter the Y coordinates:");
  scanf("%d %d",&y1,&y2);
  d=dis(x1,x2,y1,y2);
  printf("The distance between the two points:%d",d);
  return 0;
}
int dis(int a,int b,int c,int d)
{
    int r;
    r=sqrt(((b-a)*(b-a))+((d-c)*(d-c)));
    return r;
}