#include<stdio.h>
#include<math.h>
int AREA(int,int,int);
int main()
{
     int s1,s2,s3,area;
    printf("Enter the length of the sides of the triangle:");
    scanf("%d %d %d",&s1,&s2,&s3);
    area=AREA(s1,s2,s3);
    printf("The area of the triangle : %d",area);
    return 0;
}
int AREA(int a,int b,int c)
{
     int s,r;
     s=(a+b+c)/2.0;
     r=sqrt((s)*(s-a)*(s-b)*(s-c));
     return r;
}