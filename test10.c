#include <stdio.h>
void add(int *a,int *b)
{
    int sum;
    sum=*a+*b;
    printf("Sum of the integers:%d",sum);
}
void sub(int *a,int *b)
{
    int diff;
    diff=*a-*b;
    printf("Difference of the integers:%d",diff);
}
void mul(int *a,int *b)
{
    int m;
    m=(*a)*(*b);
    printf("Product of the integers:%d",m);
}
void divide(int *a,int *b)
{
    int d;
    d=(*a)/(*b);
    printf("Division of the two integers:%d",d);
}
void rem(int *a,int *b)
{
    int r;
    r=(*a)%(*b);
    printf("Remainder of the two integers :%d",r);
}
int main()
{
    int x,y,n;
    printf("Enter the two integers:");
    scanf("%d%d",&x,&y);
    printf("Operations:\n 1.Addition\n2.Substraction\n3.Multiplication\n");
    printf("4.Division\n5.Remainder\n");
    printf("Enter the option:");
    scanf("%d",&n);
    switch(n)
    {
        case 1:add(&x,&y);
               break;
        case 2:sub(&x,&y);
               break;
        case 3:mul(&x,&y);
               break;
        case 4:divide(&x,&y);
               break;
        case 5:rem(&x,&y);
               break;
        default:printf("Invalid Option");
    }
    return 0;
}